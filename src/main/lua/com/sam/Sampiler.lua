---@class Sampiler
local Sampiler = {}

-- Copy _ENV for a backup
local type, setmetatable, getmetatable = type, setmetatable, getmetatable
CopyTable = function(obj, seen)
    -- Handle non-tables and previously-seen tables.
    if type(obj) ~= 'table' then return obj end
    if seen and seen[obj] then return seen[obj] end

    -- New table; mark it as seen and copy recursively.
    local s = seen or {}
    local res = {}
    s[obj] = res
    for k, v in pairs(obj) do res[CopyTable(k, s)] = CopyTable(v, s) end
    return setmetatable(res, getmetatable(obj))
end
Backup_ENV = CopyTable(_ENV)

---@param modname string
---@return boolean|any
local import = function(modname)
    local ok, msg = pcall(require, modname)
    if not ok then
        print("Module `" .. modname .. "` failed to load.")
        print(msg)
        error("Failed to run", 0)
        -- os.exit(1)
    end
    return msg
end

package.path = package.path .. ";D:/Lua Decompiler/src/main/lua/com/sam/?.lua"
import "gg" -- GG module
import "bytecode.Bytecode" -- Bytecode engine
import "plugin.Plugin" -- Plugin module
import "prototype.Prototype52" -- Prototype unzip engine
import "compile.Compile52" -- Compile engine
import "language.Language" -- Language module
import "editor.Editor" -- Editor engine
import "config.Config" -- Config function
import "settings.Settings" -- Settings module
import "event.Event" -- Event module
import "gui.Gui" -- Gui
import "util.Steam" -- Steam

local this = Sampiler;

---檢查是否有GG模塊
function this:isGG()
    if gg == true then return false, "gg is missing as a module." end
    return true
end

function this:preinit()
    local tryGG, msg = this:isGG();
    if not tryGG then error(msg) end
end

function this:init()
    this:preinit()
    -- Bytecode version
    Bytecode:init("GG52")
    -- Get config
    Config:getSettings()
    -- Start language module
    Language:init()
    -- Setup Settings module
    Settings:init()
    -- After all loaded, load plugin module
    Plugin:init()
end

function this:newEdit()
    local input = Prompt({ Language.input.InputFile },
    { Config.Settings.inputFile }, { "file" })
    if input then
        local file = Steam:tryOpen(input[1])
        if not file then
            Alert(Language.err.InvaildFile, "")
            this:newEdit()
            return
        end
        Config:addSettings("inputFile", input[1]):saveConfig()
        Toast(
        Language.natural.Analyzing .. "\n(" .. Language.tips.IfDeviceSlow ..
        ")")
        local Proto = Protoinit(Config.Settings.inputFile)
        Editor:LoadPrototype(Proto)
        Proto, input, file = nil;
        Toast(Language.natural.AnalyzeSuccess)
        while true do
            input = Choice({
                Language.proto.ProtoHeader, Language.proto.ProtoInfo[1]
            }, nil,
            Language.natural.AnalyzeSuccess .. ", " ..
            Language.input.EditType .. "\n" .. "(" ..
            Language.tips.PressBack .. ")")
            if not input then
                this:newEdit()
                break
            end
            local extra;
            input, extra = Editor:showEditor(input)
            if input == "compile" then
                input = Editor:showOutput()
                if input then
                    SetUIVisibility(false)
                    Toast(Language.natural.OutputProcessing)
                    local ok, errMsg = pcall(Compile, extra, io.output(input))
                    extra = nil
                    if not ok then
                        print(errMsg)
                        Toast("Error while compiling!")
                        -- SetUIVisibility(true)
                        return
                    end
                    Toast(Language.natural.OutputFileSuccess)
                    input = Alert(
                    Language.natural.OutputFileSuccess .. "\n(" ..
                    input .. ")\n" ..
                    Language.tips.IsInputNewFile,
                    Language.other.Yes[1], nil, Language.other.No[1])
                    if input ~= 1 then
                        SetUIVisibility(false)
                        return
                    end
                else
                    extra = nil
                    this:newEdit()
                end
                break
            end
        end
    end
end

---@param args table
function this:main(args)
    this:init()
    while true do
        if IsUIVisibility(true) then
            SetUIVisibility(false)
            :: TriggeredBefore ::
            local mainSel = Alert("\n\n\n\n\n", Language.other.Start,
            Language.settings.Name, Language.other.Exit)
            if mainSel == 1 then
                this:newEdit()
            elseif mainSel == 2 then
                Settings.main()
            elseif mainSel == 3 then
                SetUIVisibility(true)
                break
            end
            if mainSel > 0 then goto TriggeredBefore end
        end
    end
end

Sampiler:main(arg)