--[[    Plugin name: Remove Zero Code
    Alias folder: None
    Desc: 移除全部無用區塊
    Author: Sam
--]] ---@class RemoveZeroChunk
local this = {pendingChange = false, reverse = false}
local genID
function this:init()
    genID = require("util.GUID_Gen")
    Event.add("onProtoLoaded", this.onProtoLoaded)
    Event.add("onCacheProtoZipped", this.onCacheProtoZipped)
    return {
        id = "RemoveZeroChunk",
        name = "移除無用區塊",
        desc = "移除全部無用區塊 (0-0)",
        author = "Sam",
        config = true,
        config_settings = {}
    }
end

function this:onProtoLoaded() this.pendingChange = false end

function this.onCacheProtoZipped(level)
    local proto = Editor.cacheProto[level]
    if this.pendingChange then
        for _, v in ipairs(this.pendingChange) do
            if v.level == level then
                if #proto.func > 0 then
                    for fNum = 1, #proto.func do
                        if proto.func[fNum].level == v.targetID and v.targetID ~=
                            fNum then
                            print("Update# lev: " .. level .. " orig: " ..
                                      v.targetID .. " new: " .. fNum)
                            v.targetID = fNum
                        end
                    end
                end
                if proto.instr[v.instrID][1] == "CLOSURE" then
                    if v.force then
                        -- print("Force# "..v.instrID.." to target "..v.targetID)
                        proto.instr[v.instrID][2].Bx = -v.targetID
                    elseif -proto.instr[v.instrID][2].Bx ~= v.targetID then
                        print("Change# lev: " .. level .. " To: " .. v.targetID)
                        proto.instr[v.instrID][2].Bx = -v.targetID
                    end
                end
            end
        end
    end
    return proto
end

local add_ret = function()
    local count = #Editor.protoLevel
    local curr = 1
    for num = 1, count do
        local currProto = Editor.protoLevel[num]
        if currProto.State == Editor.States.EMPTY then
            Editor.protoLevel[num].State = Editor.States.EXIST
            Editor.cacheProto[num].instr =
                {{"RETURN", {A = 0, B = 1}, id = genID(), type = {"A", "B"}}}
        end
    end
end

local del_chunk = function()
    local tmpLevel = Editor.protoLevel
    local countProto = #Editor.protoLevel
    local tmpClosureMark = {};
    for num = 1, countProto do
        if Editor.protoLevel[num].State == Editor.States.EMPTY then
            Editor.protoLevel[num].State = Editor.States.DELETED
        end
    end
    for num = 1, countProto do
        if Editor.protoLevel[num].State == Editor.States.EXIST then
            -- TODO Mark all EXIST State proto (For CLOSURE)
            local countInstr = #Editor.cacheProto[num].instr;
            for i = 1, countInstr do
                local currInstr = Editor.cacheProto[num].instr[i]
                if currInstr[1] == "CLOSURE" then
                    -- 
                    local targetProtoNum = (-currInstr[2].Bx)
                    if Editor.protoLevel[targetProtoNum] == nil then
                        print("Error ID:", targetProtoNum)
                    end
                    if Editor.protoLevel[targetProtoNum].State ==
                        Editor.States.EXIST then
                        -- tmpClosureMark
                        print("Mark# lev: " .. num .. " targetID: " ..
                                  targetProtoNum .. " instrID: " .. i)
                        tmpClosureMark[#tmpClosureMark + 1] =
                            {
                                level = num,
                                targetID = targetProtoNum,
                                instrID = i
                            }
                    elseif Editor.protoLevel[targetProtoNum].State ==
                        Editor.States.DELETED then
                        tmpClosureMark[#tmpClosureMark + 1] =
                            {
                                level = num,
                                targetID = 1,
                                instrID = i,
                                force = true
                            }
                    end
                end
            end
        end
    end
    -- TODO Get CLOSURE back to right position
    this.pendingChange = tmpClosureMark
end

function this:main()
    if this.pendingChange ~= false then
        Toast("Error: Already finished!")
        return
    end
    this.reverse = false
    while true do

        local action = Choice({
            "反向模式: " ..
                Language.other[this.reverse == true and "Yes" or "No"][1],
            "開始刪除"
        }, nil,
                              "反向模式: 在空區塊添加RETURN 0 1來避免刪除區塊帶來的問題")
        if action == 1 then
            this.reverse = not this.reverse
        elseif action == 2 then
            if this.reverse then
                add_ret()
            else
                del_chunk()
            end
            break
        else
            this.reverse = nil
            return
        end
    end
    Toast("Finish!")
    return "close_plugin_UI"
end

return this
