--[[    Plugin name: Decompile
    Alias folder: decompile/*
    Desc: 反編譯
    Author: Sam
--]]
---@class Decompile
local this = {}

local OPCode = require "plugin.decompile.static.opcode.Opcode"
local JumpPointer = require "plugin.decompile.static.block.JumpPointer"
local LoopBlock = require "plugin.decompile.static.block.LoopBlock"
local LogicBlock = require "plugin.decompile.static.block.LogicBlockv2"
local FixJump = require "plugin.decompile.static.block.FixJumps" --? Maybe didn't need

function this:init()
    return {
        id = "Decompile",
        name = "反編譯",
        desc = "還原成可閲讀代碼",
        author = "Sam",
        config = true,
        config_settings = {
            lastDecompMode = nil,
            lastDecompRange = 1, -- 1: Only this chunk | 2: Whole chunk
        },
        language = false,
        languages = {
            ZH_CN_S = "",
            ZH_CN_T = "",
            ENG = ""
        }
    }
end

function this:startDecomp(mode)
    local cache = {}
    cache.proto = Editor.cacheProto[Editor.currentLevel]
    -- First, load loop block and logic block
    cache.jumpPointer = JumpPointer(cache.proto)
    cache.loopBlock = LoopBlock(cache.proto)
    cache.logicBlock = LogicBlock(cache.proto, cache.loopBlock, cache.jumpPointer)
    -- Ok, we can init the @ENV now
    local lenInstr = #cache.proto.instr
    cache.pc = 1
    DecompOutput = {
        out = "",
        add = function(...)
            local out = { ... }
            for i in ipairs(out) do
                if type(out[i]) ~= "string" then
                    out[i] = tostring(out[i])
                end
            end
            DecompOutput.out = DecompOutput.out .. "\n[" .. cache.pc .. "] " .. table.concat(out, " ") .. "\n"
        end,
        dump = function()
            local out = "Decompile finish\n" .. DecompOutput.out
            DecompOutput = nil;
            return out
        end
    }
    while cache.pc < lenInstr do
        local nextPC, pcNum = OPCode:handleOP(cache.proto.instr[cache.pc], cache)
        if nextPC then
            cache.pc = pcNum
        else
            cache.pc = cache.pc + 1
        end
    end
    cache = nil
    Alert(DecompOutput.dump())
end

function this:main()
    if (Editor.protoLevel[Editor.currentLevel].State == Editor.States.DELETED or
    Editor.protoLevel[Editor.currentLevel].State == Editor.States.EMPTY)
    then
        Toast("Chunk is empty or deleted!")
        return
    end
    local mode = {
        "靜態反編譯",
        "==============="
    }
    local range = Config.Settings.lastDecompRange;
    -- Range: Only this chunk, Whole chunk
    while true do
        mode[3] = "範圍: " .. (range == 1 and "只有當前區塊" or "整個脚本")
        local sel_mode = Choice(mode, Config.Settings.lastDecompMode, "反編譯選項")
        if not sel_mode or sel_mode == 0 then
            break
        elseif sel_mode > 0 then
            if sel_mode == 1 then
                Config:addSettings("lastDecompRange", range)
                :addSettings("lastDecompMode", sel_mode)
                :saveConfig()
                this:startDecomp(sel_mode)
            elseif sel_mode == 3 then -- range
                range = range == 1 and 2 or 1
            end
        end
    end
end

return this