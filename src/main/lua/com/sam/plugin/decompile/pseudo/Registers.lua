local Declaration = require("plugin.decompile.pseudo.Declaration")

---@class Registers
local Registers = {}

function Registers:new(registers, length, declList, f)
    local obj = {}
    obj.registers = registers
    obj.length = length
    obj.decls = Declaration:new()
end