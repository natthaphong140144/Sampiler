local reduceJMP
---@param self LiteVM
---@param add number
reduceJMP = function(self, add)
    add = add or 0
    local pc = self.pc
    if add then pc = pc + add end
    local cond = self.instr[pc][1] == "JMP"
    local sBx = self.instr[pc][2].sBx or 0
    if cond then
        sBx = sBx + 1
        self.skip[pc + 1] = true
        if pc <= self.maxPC and self.instr[pc + 1][1] == "JMP" then
            return reduceJMP(self, 1 + add)
        end
    end
    return sBx
end
local reduceRETURN
---@param self LiteVM
---@param add number
reduceRETURN = function(self, add)
    add = add or 0
    local pc = self.pc
    if add then pc = pc + add end
    local cond = pc <= self.maxPC and self.instr[pc][1] == "RETURN"
    if cond then
        self.skip[pc + 1] = true
        reduceRETURN(self, 1 + add)
    end
end
---@type fun(self:LiteVM):string
local shortEXPR_TAB_UP
---@param self LiteVM
---@param add number
---@param last_OP string
shortEXPR_TAB_UP = function(self, add, last_OP, last_A)
    -- Merge GETTABUP > (Short) GETTABLE
    add = add or 0
    local pc = self.pc
    if add then pc = pc + add end
    local OP = self.instr[pc - 1][1] -- self OP
    local A, C = self.instr[pc - 1][2].A, self.instr[pc - 1][2].C
    local cond = pc <= self.maxPC and self.instr[pc][1] == "GETTABLE" -- got sub
    print("OP", OP)
    if OP == "GETTABUP" and (last_OP ~= "GETTABLE" or last_OP ~= "GETTABUP") then -- root
        if cond then
            return shortEXPR_TAB_UP(self, 1, OP, A)
        end
    elseif OP == "GETTABLE" and A == last_A then -- sub
        print("    SUB")
        self.skip[pc] = true
        local nextInstr = ""
        if cond then
            nextInstr = shortEXPR_TAB_UP(self, add + 1, OP, A) or ""
        end
        return ("[%s]"):format(self.getRK(C)) .. nextInstr
    end
end
local toPseudo = {
    -- Loading constants (加载参数)
    ---@param self LiteVM
    LOADK = function(self, args) -- [1] LOADK((A,Bx)) | R(A) := Kst(Bx)
        local format = "R(A) := Kst(Bx)"
        local const = self.getConst(args.Bx)
        return ("%s = %s -- index: %i"):format(self.getLocalName(args.A), tostring(const), -args.Bx), format
    end,
    ---@param self LiteVM
    LOADKX = function(self, args) -- [2] LOADKX((A)) | R(A) := Kst(extra arg)
        local format = "R(A) := Kst(extra arg)"
        if self.pc + 1 > self.maxPC or self.instr[self.pc][1] ~= "EXTRAARG" then
            return "-- Invaild LOADKX", format
        end
        self.skip[self.pc + 1] = true
        local extra_arg = self.instr[self.pc][2].Ax
        local const = self.getConst(extra_arg)
        return ("%s = %s -- index: %i"):format(self.getLocalName(args.A), tostring(const), -extra_arg), format
    end,
    ---@param self LiteVM
    EXTRAARG = function(self, args) -- [39] EXTRAARG((Ax)) | extra (larger) argument for previous opcode
        local format = "extra (larger) argument for previous opcode"
        return ("-- EXTRAARG [%s]"):format(args.Ax), format
    end,
    -- Unary functions (一元函数)
    ---@param self LiteVM
    MOVE = function(self, args) -- [0] MOVE((A,B)) | R(A) := R(B)
        local format = "R(A) := R(B)"
        return ("%s = %s"):format(self.getLocalName(args.A), self.getLocalName(args.B)), format
    end,
    ---@param self LiteVM
    UNM = function(self, args) -- [19] UNM((A,B)) | R(A) := -R(B)
        local format = "R(A) := -R(B)"
        return ("%s = -%s"):format(self.getLocalName(args.A), self.getLocalName(args.B)), format
    end,
    ---@param self LiteVM
    NOT = function(self, args) -- [20] NOT((A,B)) | R(A) := not R(B)
        local format = "R(A) := not R(B)"
        return ("%s = not %s"):format(self.getLocalName(args.A), self.getLocalName(args.B)), format
    end,
    ---@param self LiteVM
    BNOT = function(self, args) -- [26/41] BNOT((A,B)) | R(A) := ~ R(B)
        local format = "R(A) := ~ R(B)"
        return ("%s = ~ %s"):format(self.getLocalName(args.A), self.getLocalName(args.B)), format
    end,
    ---@param self LiteVM
    LEN = function(self, args) -- [21] LEN((A,B)) | R(A) := length of R(B)
        local format = "R(A) := length of R(B)"
        return ("%s = #%s"):format(self.getLocalName(args.A), self.getLocalName(args.B)), format
    end,
    -- Binary functions (二元函数)
    ---@param self LiteVM
    ADD = function(self, args) -- [13] ADD((A,B,C)) | R(A) := RK(B) + RK(C)
        local format = "R(A) := RK(B) + RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s + %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    SUB = function(self, args) -- [14] SUB((A,B,C)) | R(A) := RK(B) - RK(C)
        local format = "R(A) := RK(B) - RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s - %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    MUL = function(self, args) -- [15] MUL((A,B,C)) | R(A) := RK(B) * RK(C)
        local format = "R(A) := RK(B) * RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s * %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    DIV = function(self, args) -- [16] DIV((A,B,C)) | R(A) := RK(B) / RK(C)
        local format = "R(A) := RK(B) / RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s * %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    MOD = function(self, args) -- [17] MOD((A,B,C)) | R(A) := RK(B) % RK(C)
        local format = "R(A) := RK(B) % RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s %% %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    POW = function(self, args) -- [18] POW((A,B,C)) | R(A) := RK(B) ^ RK(C)
        local format = "R(A) := RK(B) ^ RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s ^ %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    IDIV = function(self, args) -- [19/40] IDIV((A,B,C)) | R(A) := RK(B) // RK(C)
        local format = "R(A) := RK(B) // RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s // %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    BAND = function(self, args) -- [20/42] BAND((A,B,C)) | R(A) := RK(B) & RK(C)
        local format = "R(A) := RK(B) & RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s & %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    BOR = function(self, args) -- [21/43] BOR((A,B,C)) | R(A) := RK(B) | RK(C)
        local format = "R(A) := RK(B) | RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s | %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    BXOR = function(self, args) -- [22/44] BXOR((A,B,C)) | R(A) := RK(B) ~ RK(C)
        local format = "R(A) := RK(B) ~ RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s ^ %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    SHL = function(self, args) -- [23/45] SHL((A,B,C)) | R(A) := RK(B) << RK(C)
        local format = "R(A) := RK(B) << RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s << %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    SHR = function(self, args) -- [24/46] SHR((A,B,C)) | R(A) := RK(B) >> RK(C)
        local format = "R(A) := RK(B) >> RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s = %s >> %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    -- Table access (表格处理)
    ---@param self LiteVM
    GETTABLE = function(self, args) -- [7] GETTABLE((A,B,C)) | R(A) := R(B)[RK(C)]
        local format = "R(A) := R(B)[RK(C)]"
        local RK_C = self.getRK(args.C)
        return ("%s = %s[%s]"):format(self.getLocalName(args.A), self.getLocalName(args.B), RK_C), format
    end,
    ---@param self LiteVM
    SETTABLE = function(self, args) -- [10] SETTABLE((A,B,C)) | R(A)[RK(B)] := RK(C)
        local format = "R(A)[RK(B)] := RK(C)"
        local RK_B = self.getRK(args.B)
        local RK_C = self.getRK(args.C)
        return ("%s[%s] = %s"):format(self.getLocalName(args.A), RK_B, RK_C), format
    end,
    ---@param self LiteVM
    NEWTABLE = function(self, args) -- [11] NEWTABLE((A,B,C)) | R(A) := {} (size = B,C)
        local format = "R(A) := {} (size = B,C)"
        return ("%s = {}"):format(self.getLocalName(args.A)), format
    end,
    ---@param self LiteVM
    SELF = function(self, args) -- [12] SELF((A,B,C)) | R(A+1) := R(B); R(A) := R(B)[RK(C)]
        local format = "R(A+1) := R(B); R(A) := R(B)[RK(C)]"
        -- %s = %s; %s = %s[%s]
        local RK_C = self.getRK(args.C)
        return ("%s = %s; %s = %s[%s]")
        :format(self.getLocalName(args.A + 1), self.getLocalName(args.B), self.getLocalName(args.A), self.getLocalName(args.B), RK_C), format
    end,
    ---@param self LiteVM
    SETLIST = function(self, args) -- [36] SETLIST((A,B,C)) | R(A)[(C-1)*FPF+i] := R(A+i), 1 <= i <= B)
        local format = "R(A)[(C-1)*FPF+i] := R(A+i), 1 <= i <= B)"
        local A, B, C = args.A, args.B, args.C
        if C == 0 then
            if self.pc + 1 > self.maxPC or self.instr[self.pc][1] ~= "EXTRAARG" then
                return "-- Invaild SETLIST", format
            end
            self.skip[self.pc + 1] = true
            C = -self.instr[self.pc][2].Ax
        elseif C < 0 then
            C = 255 + (-C)
        end
        if B == 0 then
            B = self.reg - A - 1
        end
        local table_regs = {}
        for i = B, 1, -1 do
            table_regs[i] = ("%s[%i] = %s"):format(self.getLocalName(A), (C - 1) * 50 + i, self.getLocalName(A + i))
        end
        return table.concat(table_regs, "; "), format
    end,
    -- Dealing with tuples (元组处理)
    ---@param self LiteVM
    LOADNIL = function(self, args, root) -- [4] LOADNIL((A,B)) | R(A), R(A+1), ..., R(A+B) := nil
        local format = "R(A), R(A+1), ..., R(A+B) := nil"
        local table_nil = {}
        for i = args.A, args.A + args.B do
            table_nil[#table_nil + 1] = self.getLocalName(i)
        end
        return ("%s%s = nil"):format(root == true and "local " or "", table.concat(table_nil, ", ")), format
    end,
    ---@param self LiteVM
    CONCAT = function(self, args) -- [22] CONCAT((A,B,C)) | R(A) := R(B).. ... ..R(C)
        local format = "R(A) := R(B).. ... ..R(C)"
        local table_regs = {}
        for i = args.B, args.C do
            table_regs[#table_regs + 1] = self.getLocalName(i)
        end
        return ("%s = %s"):format(self.getLocalName(args.A), table.concat(table_regs, " .. ")), format
    end,
    ---@param self LiteVM
    CALL = function(self, args) -- [29] CALL((A,B,C)) | R(A), ... ,R(A+C-2) := R(A)(R(A+1), ... ,R(A+B-1))
        local format = "R(A), ... ,R(A+C-2) := R(A)(R(A+1), ... ,R(A+B-1))"
        local A, B, C = args.A, args.B, args.C
        local multiple = C >= 3 or C == 0
        if B == 0 then B = self.reg - A end
        if C == 0 then C = self.reg - A + 1 end
        local func = self.getLocalName(A)
        local arg = {}
        for register = A + 1, A + B - 1 do
            arg[#arg + 1] = self.getLocalName(register)
        end
        local result
        if C == 1 then -- No return value
            result = ("%s(%s)"):format(func, table.concat(arg, ", "))
        else
            if args.C == 2 and not multiple then
                result = ("%s = %s(%s)"):format(self.getLocalName(A), func, table.concat(arg, ", "))
            else
                local regs = {}
                for register = A, A + C - 2 do
                    regs[#regs + 1] = self.getLocalName(register)
                end
                result = ("%s = %s(%s)"):format(table.concat(regs, ", "), func, table.concat(arg, ", "))
            end
        end
        return result, format
    end,
    ---@param self LiteVM
    RETURN = function(self, args) -- [31] RETURN((A,B)) | return R(A), ... ,R(A+B-2) (see note)
        local format = "return R(A), ... ,R(A+B-2) (see note)"
        local A, B = args.A, args.B
        if B == 0 then B = self.reg - A + 1 end
        local table_ret = {}
        for register = A, A + B - 2 do
            table_ret[#table_ret + 1] = self.getLocalName(register)
        end
        reduceRETURN(self)
        if self.pc >= self.maxPC and #table_ret == 0 then
            return "", format
        end
        return (";do return %s end;"):format(table.concat(table_ret, ", ")), format
    end,
    ---@param self LiteVM
    VARARG = function(self, args) -- [38] VARARG((A,B)) |  R(A), R(A+1), ..., R(A+B-2) = vararg
        local format = " R(A), R(A+1), ..., R(A+B-2) = vararg"
        local A, B = args.A, args.B
        local multiple = B ~= 2
        if B == 1 then return "-- Invaild VARARG", format end
        if B == 0 then B = self.reg - A + 1 end
        local vararg = {}
        for register = A, A + B - 2 do
            vararg[#vararg + 1] = self.getLocalName(register)
        end
        return ("%s = ..."):format(table.concat(vararg, ", ")), format
    end,
    ---@param self LiteVM
    TAILCALL = function(self, args) -- [30] TAILCALL((A,B,C)) |  return R(A)(R(A+1), ... ,R(A+B-1))
        local format = " return R(A)(R(A+1), ... ,R(A+B-1))"
        local A, B = args.A, args.B
        if B == 0 then B = self.reg - A end
        local func = self.getLocalName(args.A)
        local arg = {}
        for register = A + 1, A + B - 1 do
            arg[#arg + 1] = self.getLocalName(register)
        end
        reduceRETURN(self)
        return (";do return %s(%s) end;"):format(func, table.concat(arg, ", ")), format
    end,
    ---@param self LiteVM
    TFORCALL = function(self, args) -- [34] TFORCALL((A,C)) |  R(A+3), ... ,R(A+2+C) := R(A)(R(A+1), R(A+2));
        local format = " R(A+3), ... ,R(A+2+C) := R(A)(R(A+1), R(A+2));"
        local table_ret = {}
        for register = args.A + 3, args.A + 2 + args.C do
            table_ret[#table_ret + 1] = self.getLocalName(register)
        end
        return ("%s = %s(%s, %s)")
        :format(table.concat(table_ret, ", "), self.getLocalName(args.A), self.getLocalName(args.A + 1), self.getLocalName(args.A + 2)), format
    end,
    -- Interaction with upvalues (与外部局部变量的交互)
    ---@param self LiteVM
    GETUPVAL = function(self, args, trace) -- [5] GETUPVAL((A,B)) | R(A) := UpValue
        local format = "R(A) := UpValue"
        local B = args.B + 1
        local request = self.upval[B]
        if not request then return "-- Invaild GETUPVAL", format end
        local value
        if trace then
            value = trace(self, self.level, request)
        else
            value = "U_" .. B .. " -- Enable global range to unlock full feature"
        end
        return ("%s = %s"):format(self.getLocalName(args.A), value), format
    end,
    ---@param self LiteVM
    SETUPVAL = function(self, args, trace) -- [9] SETUPVAL((A,B)) | UpValue := R(A)
        local format = "UpValue := R(A)"
        local B = args.B + 1
        local request = self.upval[B]
        if not request then return "-- Invaild SETUPVAL", format end
        local value
        if trace then
            value = trace(self, self.level, request)
        else
            value = "U_" .. B
        end
        return ("%s = %s"):format(value, self.getLocalName(args.A)), format
    end,
    ---@param self LiteVM
    GETTABUP = function(self, args, trace) -- [6] GETTABUP((A,B,C)) | R(A) := UpValue[RK(C)]
        local format = "R(A) := UpValue[RK(C)]"
        local B = args.B + 1
        local request = self.upval[B]
        if not request then return "-- Invaild GETTABUP", format end
        local value
        if trace then
            value = trace(self, self.level, request)
        else
            value = "U_" .. B
        end
        local short = shortEXPR_TAB_UP(self) or ""
        print("R", short)
        return ("%s = %s[%s]%s"):format(self.getLocalName(args.A), value, self.getRK(args.C), short), format
    end,
    ---@param self LiteVM
    SETTABUP = function(self, args, trace) -- [8] SETTABUP((A,B,C)) | UpValue[A][RK(B)] := RK(C)
        local format = "UpValue[A][RK(B)] := RK(C)"
        local A = args.A + 1
        local request = self.upval[A]
        if not request then return "-- Invaild SETTABUP", format end
        local value
        if trace then
            value = trace(self, self.level, request)
        else
            value = "U_" .. A
        end
        return ("%s[%s] = %s"):format(value, self.getRK(args.B), self.getRK(args.C)), format
    end,
    -- Logical functions (逻辑函数)
    ---@param self LiteVM
    LOADBOOL = function(self, args) -- [3] LOADBOOL((A,B,C)) | R(A) := (Bool)B; if (C) pc++
        local format = "R(A) := (Bool)B; if (C) pc++"
        local jump = ""
        if args.C > 0 then -- pc ++
            jump = self.jump(self.pc, self.pc + 1)
        end
        local bool = args.B > 0 and "true" or "false"
        return ("%s = %s %s"):format(self.getLocalName(args.A), bool, jump), format
    end,
    ---@param self LiteVM
    EQ = function(self, args) -- [24] EQ((A,B,C)) | if ((RK(B) == RK(C)) ~= A) then pc++
        local format = "if ((RK(B) == RK(C)) ~= A) then pc++"
        local boolA = tostring(args.A == 0)
        local expTrue = self.jump(self.pc, self.pc + reduceJMP(self))
        local expFalse = self.jump(self.pc, self.pc + reduceJMP(self, 1))
        local cond_result = "%s else %s"
        if expTrue == expFalse then cond_result = "%s" end
        return ("if (%s == %s) == %s then " .. cond_result .. " end")
        :format(self.getRK(args.B), self.getRK(args.C), boolA, expTrue, expFalse), format
    end,
    ---@param self LiteVM
    LT = function(self, args) -- [25] LT((A,B,C)) | if ((RK(B) < RK(C)) ~= A) then pc++
        local format = "if ((RK(B) < RK(C)) ~= A) then pc++"
        local boolA = tostring(args.A ~= 0)
        local boolA = args.A == 0 and "<" or ">"
        local expTrue = self.jump(self.pc, self.pc + reduceJMP(self))
        local expFalse = self.jump(self.pc, self.pc + reduceJMP(self, 1))
        local cond_result = "%s else %s"
        if expTrue == expFalse then cond_result = "else %s" end
        return ("if %s %s %s then " .. cond_result .. " end")
        :format(self.getRK(args.B), boolA, self.getRK(args.C), expTrue, expFalse), format
    end,
    ---@param self LiteVM
    LE = function(self, args) -- [26] LE((A,B,C)) | if ((RK(B) <= RK(C)) ~= A) then pc++
        local format = "if ((RK(B) <= RK(C)) ~= A) then pc++"
        local boolA = tostring(args.A == 0)
        local boolA = args.A == 0 and "<=" or ">="
        local expTrue = self.jump(self.pc, self.pc + reduceJMP(self))
        local expFalse = self.jump(self.pc, self.pc + reduceJMP(self, 1))
        local cond_result = "%s else %s"
        if expTrue == expFalse then cond_result = "%s" end
        return ("if %s %s %s then " .. cond_result .. " end")
        :format(self.getRK(args.B), boolA, self.getRK(args.C), expTrue, expFalse), format
    end,
    ---@param self LiteVM
    TEST = function(self, args) -- [27] TEST((A,C)) | if not (R(A) <=> C) then pc++
        local format = "if not (R(A) <=> C) then pc++"
        local boolC = tostring(args.C ~= 0)
        local expTrue = self.jump(self.pc, self.pc + reduceJMP(self))
        local expFalse = self.jump(self.pc, self.pc + reduceJMP(self, 1))
        local cond_result = "%s else %s"
        if expTrue == expFalse then cond_result = "%s" end
        return ("if (not %s) == %s then " .. cond_result .. " end")
        :format(self.getRK(args.A), boolC, expTrue, expFalse), format
    end,
    ---@param self LiteVM
    TESTSET = function(self, args) -- [28] TESTSET((A,B,C)) | if (R(B) <=> C) then R(A) := R(B) else pc++
        local format = "if (R(B) <=> C) then R(A) := R(B) else pc++"
        local boolC = tostring(args.C ~= 0)
        local expFalse = self.jump(self.pc, self.pc + reduceJMP(self))
        return ("if (not %s) == %s then %s = %s %s end")
        :format(self.getRK(args.B), boolC, self.getRK(args.A), self.getRK(args.B), expFalse), format
    end,
    -- Branches, loops and closures (分支、循环和闭包)
    ---@param self LiteVM
    JMP = function(self, args, root) -- [23] JMP((A,sBx)) | pc+=sBx; if (A) close all upvalues >= R(A) + 1
        local format = "pc+=sBx; if (A) close all upvalues >= R(A) + 1"
        -- TODO Global range required
        local jmp = self.jump(self.pc, self.pc + args.sBx)
        local isClose = args.A > 0 and "-- Enable global range to unlock full feature" or ""
        return ("%s %s"):format(jmp, isClose), format
    end,
    ---@param self LiteVM
    FORLOOP = function(self, args) -- [32] FORLOOP((A,sBx)) | R(A)+=R(A+2); if R(A) <?= R(A+1) then { pc+=sBx; R(A+3)=R(A) }
        local format = "R(A)+=R(A+2); if R(A) <?= R(A+1) then { pc+=sBx; R(A+3)=R(A) }"
        return (
        ";do local step = %s; %s = %s + step; local idx = %s; local limit = %s;" ..
        "if (step > 0 and idx <= limit) or (step < 0 and limit <= idx) then %s = %s %s end end;"
        )
        :format(
        self.getLocalName(args.A + 2), self.getLocalName(args.A), self.getLocalName(args.A), self.getLocalName(args.A), self.getLocalName(args.A + 1),
        self.getLocalName(args.A + 3), self.getLocalName(args.A), self.jump(self.pc, self.pc + args.sBx)
        ), format
    end,
    ---@param self LiteVM
    FORPREP = function(self, args) -- [33] FORPREP((A,sBx)) | R(A)-=R(A+2); pc+=sBx
        local format = "R(A)-=R(A+2); pc+=sBx"

        return ("%s = %s - %s; %s")
        :format(self.getLocalName(args.A), self.getLocalName(args.A), self.getLocalName(args.A + 2), self.jump(self.pc, self.pc + args.sBx)), format
    end,
    ---@param self LiteVM
    TFORLOOP = function(self, args) -- [35] TFORLOOP((A,sBx)) | if R(A+1) ~= nil then { R(A)=R(A+1); pc += sBx }
        local format = "if R(A+1) ~= nil then { R(A)=R(A+1); pc += sBx }"
        return ("if %s ~= nil then %s = %s %s end")
        :format(self.getLocalName(args.A + 1), self.getLocalName(args.A), self.getLocalName(args.A + 1), self.jump(self.pc, self.pc + args.sBx)), format
    end,
    ---@param self LiteVM
    CLOSURE = function(self, args) -- [37] CLOSURE((A,Bx)) | R(A) := closure(KPROTO[Bx])
        local format = "R(A) := closure(KPROTO[Bx])"
        local label = self.func[-args.Bx]
        if not label then
            label = { id = "nil" }
        end
        return ("%s = FUNC_%s -- Proto: %s"):format(self.getLocalName(args.A), label.id, -args.Bx), format
    end,
    -- Specials (特殊)
    ---@param self LiteVM
    UNKNOWN = function(self, args) -- [??] UNKNOWN(Ax)
        local format = "???"
        return ("-- UNKNOWN [%s]"):format(args.Ax), format
    end
}

return toPseudo