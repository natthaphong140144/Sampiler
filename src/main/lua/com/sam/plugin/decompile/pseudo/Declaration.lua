---@class Declaration
local Declaration = {
    name = "";
    begin = 0;
    ends = 0;
    is_arg = false;
}

function Declaration:new(name, begin, ends, arg)
    local obj = {}
    setmetatable(obj, self)
    self.__index = self
    obj.name = name
    obj.begin = begin
    obj.ends = ends
    obj.is_arg = arg
    return obj
end

return Declaration