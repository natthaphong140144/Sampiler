local log = require "util.Log"("WARN")
local pc = 1
local globalTree
local blocks = {}
local while_block = {}
local loop_block = {}
local ignoreInstr = {}
-- local reg_JMP = {}
-- local JMP_Pointer;
local logic_op = {
    EQ = 1,
    LT = 1,
    LE = 1,
    TEST = 1,
    TESTSET = 1
}
-- reduce global fetch
local string, pairs = string, pairs

local string_find = string.find
--
-- local -- regJMP = function(instr_num)
-- reg_JMP[instr_num] = true
-- end
local getId = function(instr_num)
    return globalTree.instr[instr_num] and globalTree.instr[instr_num].id or 'null'
end

local ignore = function(instr)
    ignoreInstr[instr] = true
end

local addType = function(type, arg)
    local target = {}
    for name, instr_num in pairs(arg) do
        if not string_find(name, '[sS]ize') then
            target[name] = getId(instr_num)
        end
    end
    arg.type = type
    arg.target = target
    blocks[#blocks + 1] = arg
end

local parseLogic = function(instr, OP, A, B, C)
    log.debug('检测到block (指令行) ' .. (pc - 1) .. ', 逻辑类型 ' .. (OP) .. ')')
    local isJMP = instr[pc][1] == 'JMP'
    if not isJMP then
        log.warn('非法逻辑block: `JMP` excepted, got `' .. instr[pc][1] .. '`')
        return 'broken_logic', {
            _blockStart = pc - 2,
            _start = pc - 1,
        }-- throw new IllegalBlockException();
    end
    ignore(pc - 1)
    local OP_JMP = instr[pc][2].sBx
    if OP_JMP < 0 or pc + OP_JMP < 1 or pc + OP_JMP > #instr then
        if
        while_block[pc + OP_JMP + 1] and instr[while_block[pc + OP_JMP + 1] - 1][1] == 'JMP' and
        while_block[pc + OP_JMP + 1] + instr[while_block[pc + OP_JMP + 1] - 1][2].sBx == pc - 2
        then
            log.info('block类型: while block (cond A)')
            -- print(' While-in-while: while detected.')
            while_block[pc - 2] = while_block[pc + OP_JMP + 1] - 1
            -- regJMP(pc) -- After Logic
            -- regJMP(while_block[pc + OP_JMP + 1] - 1) -- End
            return 'while', {
                _blockStart = pc - 2,
                _start = pc - 1,
                _end = while_block[pc + OP_JMP + 1] - 1
            }
        elseif while_block[pc + OP_JMP + 1] then
            -- print('If block detected')
            -- print(" Target is logic_if, and it's in a while block.")
            local tail = instr[while_block[pc + OP_JMP + 1] - 1]
            if tail[1] == 'JMP' and while_block[while_block[pc + OP_JMP + 1] + tail[2].sBx] then
                --* empty else
                log.info('block类型: empty else block (cond A)')
                -- regJMP(pc)
                -- regJMP(while_block[pc + OP_JMP + 1] - 1)
                return 'logic_if_else', {
                    _blockStart = pc - 2,
                    _start = pc - 1,
                    _size = while_block[pc + OP_JMP + 1] - 2 - pc,
                    _else = while_block[pc + OP_JMP + 1] - 1,
                    _elseSize = 0,
                    _end = while_block[pc + OP_JMP + 1]
                }
            else
                --* normal if
                log.info('block类型: if block (cond A)')
                -- regJMP(pc)
                return 'logic_if', {
                    _blockStart = pc - 2,
                    _start = pc - 1,
                    _end = while_block[pc + OP_JMP + 1]
                }
            end
        else
            log.info('block类型: repeat block (cond A)')
            -- print('Repeat block detected')
            --! TODO: check while true block
            return 'repeat', {
                _blockStart = pc - 2,
                _start = pc + OP_JMP + 1,
                _end = pc
            }
        end
    end
    local haveTail = instr[pc + OP_JMP][1] == 'JMP'
    local tailInstr = pc + OP_JMP
    if haveTail then
        -- regJMP(tailInstr)
        if tailInstr + instr[tailInstr][2].sBx + 1 == pc - 2 then -- while
            log.info('block类型: while block (cond B)')
            while_block[pc - 2] = tailInstr
            -- regJMP(pc)
            return 'while', {
                _blockStart = pc - 2,
                _start = pc - 1,
                _end = tailInstr
            }
        else
            if while_block[tailInstr + instr[tailInstr][2].sBx + 1] then -- if_else in while
                log.info('block类型: if else block (cond B)')
                -- regJMP(pc)
                return 'logic_if_else', {
                    _blockStart = pc - 2,
                    _start = pc - 1,
                    _size = OP_JMP - 1,
                    _else = tailInstr,
                    _elseSize = while_block[tailInstr + instr[tailInstr][2].sBx + 1] - tailInstr - 1,
                    _end = while_block[tailInstr + instr[tailInstr][2].sBx + 1]
                }
            elseif loop_block[tailInstr + instr[tailInstr][2].sBx + 1] then -- if in loop
                --?  determine if and if else block
                log.debug('test else instr:', tailInstr, pc)
                if tailInstr == pc then -- empty if block
                    log.info('block类型: empty if block (cond B-1)')
                else
                    log.info('block类型: if else block (cond B-1)')
                end
                -- regJMP(pc)
                -- regJMP(pc+OP_JMP)
                return 'logic_if_else', {
                    _blockStart = pc - 2,
                    _start = pc - 1,
                    _size = OP_JMP - 1,
                    _else = pc + OP_JMP,
                    _elseSize = instr[tailInstr][2].sBx,
                    _end = tailInstr
                }
                -- elseif instr[tailInstr][2].sBx+tailInstr+1 == pc-1 then -- while(temp)
                -- -- no blockstart
                -- log.info('block类型: while block (cond C)')
                -- while_block[pc - 1] = tailInstr
                -- -- regJMP(pc)
                -- return 'while', {
                -- _start = pc - 1,
                -- _end = tailInstr - 1
                -- }
            elseif tailInstr ~= pc then -- else block --? unclear cond
                log.debug('test else instr:', tailInstr, pc)
                log.info('block类型: if else block (cond A-2)')
                -- regJMP(pc)
                return 'logic_if_else', {
                    _blockStart = pc - 2,
                    _start = pc - 1,
                    _size = OP_JMP - 1,
                    _else = tailInstr,
                    _elseSize = instr[tailInstr][2].sBx,
                    _end = tailInstr + instr[tailInstr][2].sBx
                }
            else
                log.info('block类型: if block (cond B)')
                -- regJMP(pc)
                -- print(' - Block size: ', instr[tailInstr][2].sBx)
                return 'logic_if', {
                    _blockStart = pc - 2,
                    _start = pc - 1,
                    _end = tailInstr + instr[tailInstr][2].sBx
                }
            end
        end
    end
    log.info('block类型: if block (cond C)')
    -- regJMP(pc)
    return 'logic_if', {
        _blockStart = pc - 2,
        _start = pc - 1,
        _end = tailInstr
    }
end

local parseInstr = function(instr)
    local maxPC = #instr + 1
    while true do
        local Instr = instr[pc]
        pc = pc + 1
        if pc >= maxPC then
            return
        else -- handling while
            for start, _end in pairs(while_block) do
                if pc > _end then
                    while_block[start] = nil
                end
            end
        end
        local OP, A, B, C = Instr[1],
        -- A, Ax
        Instr[2].A or Instr[2].A,
        -- B, Bx, sBx
        Instr[2].B or Instr[2].Bx or Instr[2].sBx or 0,
        -- C
        Instr[2].C or 0
        if logic_op[OP] == 1 and not ignoreInstr[pc - 1] then -- Logic
            local type, arg = parseLogic(instr, OP, A, B, C)
            if type then
                addType(type, arg)
                -- print(' type:', type)
                -- print(' arg:', arg)
            end
        end
    end
end

local function Init(tree, loopBlock, jumpPointer)
    globalTree = tree
    JMP_Pointer = jumpPointer
    for i = 1, #loopBlock do
        loop_block[loopBlock[i]._end] = loopBlock[i]._start
    end
    log.info('开始解析逻辑和循环block')
    parseInstr(tree.instr)
    log.info('逻辑和循环block解析完成, 共解析了 ' .. #blocks .. ' 个block(s)')
    --log.debug("未注册的JMP",require"inspect"(reg_JMP))
    --    log.debug("未注册的JMP",require"inspect"(newTemp))
    pc, while_block, loop_block, ignoreInstr =    1, {}, {}, {}
    local ret = blocks
    blocks = {}
    return ret
end

return Init