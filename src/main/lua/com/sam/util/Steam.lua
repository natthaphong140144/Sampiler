---@class Steam
Steam = {}

local this = Steam
--- 嘗試打開脚本
---@param path string
---@return boolean
function this:tryOpen(path)
    local test = io.open(path)
    if test then
        test:close()
        return true
    else
        return false
    end
end