local log = {}
local lower = string.lower
local priority = {
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3,
    FATAL = 4
}
local level = {
    "DEBUG",
    "INFO",
    "WARN",
    "ERROR",
    "FATAL"
}
return function(logLev)
    for i = 1, #level do
        local lev = level[i]
        log[lower(lev)] = function(...)
            if priority[logLev] <= priority[lev] then
                local str = ''
                local error = error
                local write = io.write
                local ignore = function(...)
                    return write(..., "\n")
                end
                local l = {
                    DEBUG = ignore,
                    INFO = ignore,
                    WARN = ignore,
                    ERROR = error,
                    FATAL = error
                }
                for n in ipairs({ ... }) do
                    str = str .. '\t' .. tostring(({ ... })[n])
                end
                l[lev]('[' .. lev .. '] ' .. str)
            elseif priority[logLev] >= priority[lev] then
                return
            end
        end
    end
    return log
end