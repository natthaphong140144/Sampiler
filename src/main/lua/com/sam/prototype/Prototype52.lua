local Prototype;

local boolean = {[0] = 1, [1] = 1 }
local constType = { NIL = 0, BOOL = 1, NUM = 3, STR = 4 }
local numConv, convFunc = require("util.LuaNumber");
local sizeUpval;

local io = io
local io_seek
local io_read
local io_input = io.input

local string = string
local string_reverse = string.reverse
local string_byte = string.byte

local math = math
local math_ldexp = math.ldexp

local os = os
local exit = os.exit

local print = print

local totalLevel -- Start from %main

-- Useful function
local pushError = function(msg)
    print(' >> -1:' .. io_seek() .. ' ' .. msg)
    print(debug.traceback())
    exit(1)
end

local get = function(num, notreverse)
    local len = num or 1
    local str = io_read(len)
    if notreverse or Prototype.header.endian then
        return str
    end
    return string_reverse(str)
end

local getByte = function(num)
    local len = num or 1
    local cache = io_read(len)
    if not cache then pushError("Attempt to get " .. len .. " byte") end
    local ret = { string_byte(cache, 0, -1) }
    return len > 1 and ret or ret[1]
end

-------------------------------------------------------------
-- loads an integer (signed)
-------------------------------------------------------------
local getInt = function()
    local size_int = Prototype.header.int
    local ret = get(size_int)
    if not ret then
        pushError("could not load integer")
    else
        local sum = 0
        for i = size_int, 1, -1 do sum = sum * 256 + string_byte(ret, i) end
        -- test for negative number
        if string_byte(ret, size_int) > 127 then
            sum = sum - math_ldexp(1, 8 * size_int)
        end
        -- from the looks of it, integers needed are positive
        if sum < 0 then
            return 0
            -- pushError("bad integer")
        end
        return sum
    end
end

-------------------------------------------------------------
-- loads a instr
-------------------------------------------------------------
local getInstr = function()
    local instr_size = Prototype.header.instr
    local x = get(instr_size)
    if not x then
        pushError("could not load instr")
    else
        local sum = 0
        for i = instr_size, 1, -1 do sum = sum * 256 + string_byte(x, i) end
        return sum
    end
end

-------------------------------------------------------------
-- loads a size_t (assume unsigned)
-------------------------------------------------------------
local getSize = function()
    local x = get(Prototype.header.sizet)
    if not x then
        -- handled in getString()
        return
    elseif x then
        local sum = 0
        for i = Prototype.header.sizet, 1, -1 do
            sum = sum * 256 + string_byte(x, i)
        end
        return sum
    end
end

-------------------------------------------------------------
-- loads a number (lua_Number type)
-------------------------------------------------------------
local lua_NUMBER = function()
    local size_number = Prototype.header.numlen
    local x = get(size_number)
    if not x then
        pushError("could not load lua_Number")
    else
        local convert_func = convFunc
        if not convert_func then
            pushError("could not find conversion function for lua_Number")
        end
        return convert_func(x)
    end
end

local getString = function()
    local len = getSize()
    if not len then
        pushError("Could not get String Length")
    end
    if len == 0 then return '' end
    local ret = get(len - 1, true)
    --[[    local replace = {"b", "t", "n", "v", "f", "r"}
    for i = 0, 7 do
        if string_find(ret, string_char(i)) then
            ret = string_gsub(ret, string_char(i), "\\" .. i)
        end
    end
    for i = 8, 8 + #replace do
        if string_find(ret, string_char(i)) then
            ret = string_gsub(ret, string_char(i), "\\" .. replace[i - 7])
        end
    end
    --]]
    if getByte() == 0 then
        return ret
    else
        pushError('Attempt to get unfinished string')
    end
end

local setFunction = function(name, s)
    return function(...) return s[name](s, ...) end
end

local defineIO = function(steam)
    io_read = setFunction('read', steam)
    io_seek = setFunction('seek', steam)
end

local expected = function(msg, curr, orig)
    if type(orig) == 'table' then
        if not orig[curr] then
            pushError(msg .. ' expected, got (' .. curr .. ')')
        end
    elseif curr ~= orig then
        pushError(msg .. '(' .. orig .. ') expected, got (' .. curr .. ')')
    end
    return curr
end

-- Main
local readHeader = function()
    local lua_SIGNATURE = '\027Lua\x52' -- 5
    local lua_TAIL = '\x19\x93\r\n\026\n' -- 6
    local numberType = {
        ["80"] = "double", -- IEEE754 double
        ["40"] = "single", -- IEEE754 single
        ["41"] = "int", -- int
        ["81"] = "long long" -- long long
    }
    local result = {
        sign = expected('lua signature', get(5, true), lua_SIGNATURE),
        RIO = expected('offical chunk', getByte(), 0),
        endian = expected('endian/little-endian', getByte(), boolean) == 1,
        int = expected('normal integers length', getByte(), 4),
        sizet = expected('normal size_t', getByte(), {[4] = true, [8] = true }),
        instr = expected('normal instructions length', getByte(), 4),
        numlen = expected('avalible lua_NUMBER', getByte(),
        {[4] = true, [8] = true }),
        float = expected('floating point', getByte(), {[0] = true, [1] = true }),
        tail = expected('lua tail', get(6, true), lua_TAIL)
    }
    local numType = numberType[result.numlen .. result.float]
    convFunc = numConv(numType)
    result.numType = numType
    return result
end

local readPrototype
local readInstructions
local readConstants
local readFunctions
local readUpvalues
local readDebug

readPrototype = function(currLevel, rootLevel)
    totalLevel = totalLevel + 1
    ---@class Prototype
    return {
        root = rootLevel;
        level = currLevel,
        linedefined = getInt(),
        lastlinedefined = getInt(),
        param = getByte(),
        vararg = getByte(),
        reg = getByte(),
        instr = readInstructions(),
        const = readConstants(),
        func = readFunctions(totalLevel),
        upvalue = readUpvalues(),
        extra = readDebug()
    }
end

readInstructions = function()
    local len = getInt()
    local instr = {}
    if len < 1 then return instr end
    for i = 1, len do instr[i] = Bytecode:getReadable(getInstr()) end
    return instr
end

readConstants = function()
    local len = getInt()
    local const = {}
    local tmp = {}
    local types
    if len < 1 then return const end
    for i = 1, len do
        types = getByte()
        if types == constType.NIL then
            tmp = nil
        elseif types == constType.NUM then
            tmp = lua_NUMBER()
        elseif types == constType.STR then
            tmp = getString()
        elseif types == constType.BOOL then
            tmp = getByte() > 0
        else
            pushError('Unknown constant type (' .. types .. ')')
        end
        const[i] = { type = type(tmp), tmp }
    end
    return const
end

readFunctions = function(rootLevel)
    local subLevel = 1
    local len = getInt()
    local func = {}
    if len < 1 then return func end
    for i = 1, len do
        func[i] = readPrototype(subLevel, rootLevel)
        subLevel = subLevel + 1
    end
    return func
end

readUpvalues = function()
    local len = getInt()
    local upvalue = {}
    if len < 1 then
        -- Weird exception
        return upvalue
    end
    for i = 1, len do
        upvalue[i] = {
            instack = getByte() > 0, --expected('boolean', getByte(), boolean) == 1,
            position = getByte()
        }
    end
    sizeUpval = len
    return upvalue
end

readDebug = function()
    local len
    local debug = {}
    local ret = ''
    debug.source = getString()
    len = getInt()
    if len > 0 then
        debug.linenumber = {}
        for i = 1, len do debug.linenumber[i] = getInt() end
    end
    len = getInt()
    if len > 0 then
        debug.locals = {}
        for i = 1, len do
            debug.locals[i] = {
                name = getString(),
                startpc = getInt(),
                endpc = getInt()
            }
        end
    end
    len = getInt()
    if len > 0 then
        if len ~= sizeUpval then
            pushError("[DEBUG READ] Error while reading upvalue: get " .. len .. " but expected " .. sizeUpval)
        end
        debug.upval = {}
        for i = 1, len do debug.upval[i] = getString() end
    end
    return debug
end

local function main(steam)
    steam = io_input(steam)
    if not steam then
        print("Can't open steam")
        exit(1)
    end
    defineIO(steam)
    io_seek('set', 0)
    totalLevel = 0
    Prototype = { header = readHeader() }
    Prototype.main = readPrototype(totalLevel)
    Prototype.levels = totalLevel
    return Prototype
end

Protoinit = main