---@class Prompt
Prompt = gg.prompt;
---@class Choice
Choice = gg.choice;
---@class Alert
Alert = gg.alert;
---@class Toast
Toast = gg.toast;

SetUIVisibility = gg.setVisible;
IsUIVisibility = gg.isVisible;