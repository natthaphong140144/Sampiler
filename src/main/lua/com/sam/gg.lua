--[[    This is a lib for compiled GG record.

    (Useful for computer debugging)
--]]
local private = {
    visible = true
}
local storage_path = arg[0]:match('.*\\') .. "../../../../../.vscode/"
gg = {
    ['ANDROID_SDK_INT'] = 28,
    ['ASM_ARM'] = 4,
    ['ASM_ARM64'] = 6,
    ['ASM_THUMB'] = 5,
    ['BUILD'] = 99999,
    ['CACHE_DIR']    = storage_path .. 'run/cache',
    ['DUMP_SKIP_SYSTEM_LIBS'] = 1,
    ['EXT_CACHE_DIR'] = storage_path .. 'run/cache',
    ['EXT_FILES_DIR'] = storage_path .. 'run/files',
    ['EXT_STORAGE'] = storage_path .. 'run/',
    ['FILES_DIR']    = storage_path .. 'run/files',
    ['FREEZE_IN_RANGE'] = 3,
    ['FREEZE_MAY_DECREASE'] = 2,
    ['FREEZE_MAY_INCREASE'] = 1,
    ['FREEZE_NORMAL'] = 0,
    ['LOAD_APPEND'] = 8,
    ['LOAD_VALUES'] = 2,
    ['LOAD_VALUES_FREEZE'] = 3,
    ['PACKAGE'] = 'PACKAGE_NAME',
    ['POINTER_EXECUTABLE'] = 2,
    ['POINTER_EXECUTABLE_WRITABLE'] = 1,
    ['POINTER_NO'] = 4,
    ['POINTER_READ_ONLY'] = 8,
    ['POINTER_WRITABLE'] = 16,
    ['PROT_EXEC'] = 4,
    ['PROT_NONE'] = 0,
    ['PROT_READ'] = 2,
    ['PROT_WRITE'] = 1,
    ['REGION_ANONYMOUS'] = 32,
    ['REGION_ASHMEM'] = 524288,
    ['REGION_BAD'] = 131072,
    ['REGION_CODE_APP'] = 16384,
    ['REGION_CODE_SYS'] = 32768,
    ['REGION_C_ALLOC'] = 4,
    ['REGION_C_BSS'] = 16,
    ['REGION_C_DATA'] = 8,
    ['REGION_C_HEAP'] = 1,
    ['REGION_JAVA'] = 65536,
    ['REGION_JAVA_HEAP'] = 2,
    ['REGION_OTHER'] = -2080896,
    ['REGION_PPSSPP'] = 262144,
    ['REGION_STACK'] = 64,
    ['REGION_VIDEO'] = 1048576,
    ['SAVE_AS_TEXT'] = 1,
    ['SIGN_EQUAL'] = 536870912,
    ['SIGN_FUZZY_EQUAL'] = 536870912,
    ['SIGN_FUZZY_GREATER'] = 67108864,
    ['SIGN_FUZZY_LESS'] = 134217728,
    ['SIGN_FUZZY_NOT_EQUAL'] = 268435456,
    ['SIGN_GREATER_OR_EQUAL'] = 67108864,
    ['SIGN_LESS_OR_EQUAL'] = 134217728,
    ['SIGN_NOT_EQUAL'] = 268435456,
    ['TAB_MEMORY_EDITOR'] = 3,
    ['TAB_SAVED_LIST'] = 2,
    ['TAB_SEARCH'] = 1,
    ['TAB_SETTINGS'] = 0,
    ['TYPE_AUTO'] = 127,
    ['TYPE_BYTE'] = 1,
    ['TYPE_DOUBLE'] = 64,
    ['TYPE_DWORD'] = 4,
    ['TYPE_FLOAT'] = 16,
    ['TYPE_QWORD'] = 32,
    ['TYPE_WORD'] = 2,
    ['TYPE_XOR'] = 8,
    ['VERSION'] = '100.0',
    ['VERSION_INT'] = 9800,
    ---@param items table
    ---@return boolean|string
    ['addListItems'] = function(items)
        return true
    end, -- gg.addListItems(table items) -> true || string with error
    ---@param text string
    ---@param positive string
    ---@param negative string
    ---@param neutral string
    ---@return number int
    ['alert'] = function(text, positive, negative, neutral)
        if not positive then positive = 'ok' end
        print(text)
        print("[3] " .. tostring(neutral) .. "\t[1] " .. tostring(positive) .. "\t[2] " .. tostring(negative), "\n[0] Cancel")
        local input = io.read("*l")
        if not input or input == "" then
            return 0
        elseif tonumber(input) and tonumber(input) >= 0 and tonumber(input) <= 3 then
            return tonumber(input)
        else
            return 0
        end
    end, -- gg.alert(string text [, string positive = 'ok' [, string negative = nil [, string neutral = nil]]]) -> int: 0 = cancel, 1 = positive, 2 = negative, 3 = neutral
    ---@param mode number
    ---@param address number
    ---@return number|string
    ['allocatePage'] = function(mode, address) end, -- gg.allocatePage([int mode = gg.PROT_READ | gg.PROT_EXEC [, long address = 0]) -> long || string with error
    ---@param text string
    ---@param encoding string
    ---@return table
    ['bytes'] = function(text, encoding) end, -- gg.bytes(string text [, string encoding = 'UTF-8']) -> table
    ---@param items table
    ---@param selected string|nil
    ---@param message string|nil
    ---@return string|nil
    ['choice'] = function(items, selected, message)
        local count = #items
        if message then
            print(message .. "\n")
        end
        for i = 1, count do
            print(selected and selected == i and "O" or "X", ("%i %s"):format(i, items[i]))
        end
        print("Reply a number:")
        local input = io.read("*l")
        if not input or input == "" then
            return nil
        else
            return tonumber(input)
        end
    end, -- gg.choice(table items [, string selected = nil [, string message = nil]]) -> string || nil
    ---@return boolean|string
    ['clearList'] = function() end, -- gg.clearList() -> true || string with error
    ---@return nil
    ['clearResults'] = function() return nil end, -- gg.clearResults() -> nil
    ---@param from number
    ---@param to number
    ---@param bytes number
    ---@return boolean|string
    ['copyMemory'] = function(from, to, bytes) end, -- gg.copyMemory(long from, long to, int bytes) -> true || string with error
    ---@param text string
    ---@param fixLocale boolean
    ---@return nil
    ['copyText'] = function(text, fixLocale) return nil end, -- gg.copyText(string text [, bool fixLocale = true]) -> nil
    ---@param type number
    ---@param address number
    ---@param opcode number
    ---@return string
    ['disasm'] = function(type, address, opcode) end, -- gg.disasm(int type, long address, int opcode) -> string
    ---@param from number
    ---@param to number
    ---@param dir string
    ---@param flags number
    ---@return boolean|string
    ['dumpMemory'] = function(from, to, dir, flags) end, -- gg.dumpMemory(long from, long to, string dir [, int flags = nil]) -> true || string with error
    ---@param value string
    ---@param type number
    ---@return number|string
    ['editAll'] = function(value, type) end, -- gg.editAll(string value, int type) -> count of changed || string with error
    ---@return number
    ['getActiveTab'] = function() end, -- gg.getActiveTab() -> int
    ---@return string
    ['getFile'] = function()
        return arg and arg[0]
    end, -- gg.getFile() -> string
    ---@return number
    ['getLine'] = function() end, -- gg.getLine() -> int
    ---@return table|string
    ['getListItems'] = function() end, -- gg.getListItems() -> table || string with error
    ---@return string
    ['getLocale'] = function() end, -- gg.getLocale() -> string
    ---@return number
    ['getRanges'] = function() end, -- gg.getRanges() -> int
    ---@param filter string
    ---@return table
    ['getRangesList'] = function(filter) end, -- gg.getRangesList([string filter = '']) -> table
    ---@param maxCount number
    ---@param skip number
    ---@param addressMin number
    ---@param addressMax number
    ---@param valueMin string
    ---@param valueMax string
    ---@param type number
    ---@param fractional string
    ---@param pointer number
    ---@return table|string
    ['getResults'] = function(maxCount, skip, addressMin, addressMax, valueMin,
    valueMax, type, fractional, pointer) end, -- gg.getResults(int maxCount [, int skip = 0 [, long addressMin = nil [, long addressMax = nil [, string valueMin = nil [, string valueMax = nil [, int type = nil [, string fractional = nil [, int pointer = nil]]]]]]]]) -> table || string with error
    ---@return number
    ['getResultsCount'] = function() end, -- gg.getResultsCount() -> long
    ---@return table|string
    ['getSelectedElements'] = function() end, -- gg.getSelectedElements() -> table || string with error
    ---@return table|string
    ['getSelectedListItems'] = function() end, -- gg.getSelectedListItems() -> table || string with error
    ---@return table|string
    ['getSelectedResults'] = function() end, -- gg.getSelectedResults() -> table || string with error
    ---@return number
    ['getSpeed'] = function() end, -- gg.getSpeed() -> double
    ---@return table|nil
    ['getTargetInfo'] = function() end, -- gg.getTargetInfo() -> table || nil
    ---@return string|nil
    ['getTargetPackage'] = function() end, -- gg.getTargetPackage() -> string || nil
    ---@param values table
    ---@return table|string
    ['getValues'] = function(values) end, -- gg.getValues(table values) -> table || string with error
    ---@param values table
    ---@return table|string
    ['getValuesRange'] = function(values) end, -- gg.getValuesRange(table values) -> table || string with error
    ---@param address number
    ---@return nil
    ['gotoAddress'] = function(address) return nil end, -- gg.gotoAddress(long address) -> nil
    ---@return nil
    ['hideUiButton'] = function() return nil end, -- gg.hideUiButton() -> nil
    ---@return boolean|nil
    ['isClickedUiButton'] = function() end, -- gg.isClickedUiButton() -> bool || nil
    ---@param pkg string
    ---@return boolean
    ['isPackageInstalled'] = function(pkg) end, -- gg.isPackageInstalled(string pkg) -> bool
    ---@return boolean
    ['isProcessPaused'] = function() end, -- gg.isProcessPaused() -> bool
    ---@return boolean
    ['isVisible'] = function()
        print("(If you wish to let UI visible just input something...)")
        local input = io.read("*l")
        private.visible = input ~= ""
        return private.visible
    end, -- gg.isVisible() -> bool
    ---@param file string
    ---@param flags number
    ---@return boolean|string
    ['loadList'] = function(file, flags) end, -- gg.loadList(string file [, int flags = 0]) -> true || string with error
    ---@param results table
    ---@return boolean|string
    ['loadResults'] = function(results) end, -- gg.loadResults(table results) -> true || string with error
    ---@param url string
    ---@param headers table
    ---@param data string
    ---@return table|string
    ['makeRequest'] = function(url, headers, data) end, -- gg.makeRequest(string url [, table headers = {} [, string data = nil]]) -> table || string
    ---@param items table
    ---@param selection table
    ---@param message string
    ---@return table|nil
    ['multiChoice'] = function(items, selection, message) end, -- gg.multiChoice(table items [, table selection = {} [, string message = nil]]) -> table || nil
    ---@param num string
    ---@return string
    ['numberFromLocale'] = function(num) end, -- gg.numberFromLocale(string num) -> string
    ---@param num string
    ---@return string
    ['numberToLocale'] = function(num) end, -- gg.numberToLocale(string num) -> string
    ---@return boolean
    ['processKill'] = function() end, -- gg.processKill() -> bool
    ---@return boolean
    ['processPause'] = function() end, -- gg.processPause() -> bool
    ---@return boolean
    ['processResume'] = function() end, -- gg.processResume() -> bool
    ---@return boolean
    ['processToggle'] = function() end, -- gg.processToggle() -> bool
    ---@param prompts table
    ---@param defaults table
    ---@param types table
    ---@return nil|table
    ['prompt'] = function(prompts, defaults, types)
        local count = #types
        for i = 1, count do
            print("[" .. i .. "] | " .. prompts[i] .. ": " .. defaults[i] .. " | " .. types[i])
        end
        print("To return data, use Lua table format ( We will load() it )")
        local input = io.read("*l")
        if not input or input == "" then -- Cancel
            return nil
        else
            local try = load("return" .. input)
            if not try then return nil
            else return try() end
        end
    end, -- gg.prompt(table prompts [, table defaults = {} [, table types = {} ]]) -> nil || table with keys from prompts and values from inputs
    ---@param text string
    ---@param mask number
    ---@param type number
    ---@param sign number
    ---@param memoryFrom number
    ---@param memoryTo number
    ---@param limit number
    ---@return boolean|string
    ['refineAddress'] = function(text, mask, type, sign, memoryFrom, memoryTo,
    limit) end, -- gg.refineAddress(string text [, long mask = -1 [, int type = gg.TYPE_AUTO [, int sign = gg.SIGN_EQUAL [, long memoryFrom = 0 [, long memoryTo = -1 [, long limit = 0]]]]]]) -> true || string with error
    ---@param text string
    ---@param type number
    ---@param encrypted boolean
    ---@param sign number
    ---@param memoryFrom number
    ---@param memoryTo number
    ---@param limit number
    ---@return boolean|string
    ['refineNumber'] = function(text, type, encrypted, sign, memoryFrom,
    memoryTo, limit) end, -- gg.refineNumber(string text [, int type = gg.TYPE_AUTO [, bool encrypted = false [, int sign = gg.SIGN_EQUAL [, long memoryFrom = 0 [, long memoryTo = -1 [, long limit = 0]]]]]]) -> true || string with error
    ---@param items table
    ---@return boolean|string
    ['removeListItems'] = function(items) end, -- gg.removeListItems(table items) -> true || string with error
    ---@param results table
    ---@return boolean|string
    ['removeResults'] = function(results) end, -- gg.removeResults(table results) -> true || string with error
    ---@param version string
    ---@param build number
    ---@return nil
    ['require'] = function(version, build) return nil end, -- gg.require([string version = nil [, int build = 0]]) -> nil
    ---@param file string
    ---@param flags number
    ---@return boolean|string
    ['saveList'] = function(file, flags) end, -- gg.saveList(string file [, int flags = 0]) -> true || string with error
    ---@param variable table
    ---@param filename string
    ---@return boolean|string
    ['saveVariable'] = function(variable, filename) end, -- gg.saveVariable(mixed variable, string filename) -> true || string with error
    ---@param text string
    ---@param mask number
    ---@param type number
    ---@param sign number
    ---@param memoryFrom number
    ---@param memoryTo number
    ---@param limit number
    ---@return boolean|string
    ['searchAddress'] = function(text, mask, type, sign, memoryFrom, memoryTo,
    limit) end, -- gg.searchAddress(string text [, long mask = -1 [, int type = gg.TYPE_AUTO [, int sign = gg.SIGN_EQUAL [, long memoryFrom = 0 [, long memoryTo = -1 [, long limit = 0]]]]]]) -> true || string with error
    ---@param difference string
    ---@param sign number
    ---@param type number
    ---@param memoryFrom number
    ---@param memoryTo number
    ---@param limit number
    ---@return boolean|string
    ['searchFuzzy'] = function(difference, sign, type, memoryFrom, memoryTo,
    limit) end, -- gg.searchFuzzy([string difference = '0' [, int sign = gg.SIGN_FUZZY_EQUAL [, int type = gg.TYPE_AUTO [, long memoryFrom = 0 [, long memoryTo = -1 [, long limit = 0]]]]]]) -> true || string with error
    ---@param text string
    ---@param type number
    ---@param encrypted boolean
    ---@param sign number
    ---@param memoryFrom number
    ---@param memoryTo number
    ---@param limit number
    ---@return boolean|string
    ['searchNumber'] = function(text, type, encrypted, sign, memoryFrom,
    memoryTo, limit) end, -- gg.searchNumber(string text [, int type = gg.TYPE_AUTO [, bool encrypted = false [, int sign = gg.SIGN_EQUAL [, long memoryFrom = 0 [, long memoryTo = -1 [, long limit = 0]]]]]]) -> true || string with error
    ---@param ranges number
    ---@return nil
    ['setRanges'] = function(ranges) return nil end, -- gg.setRanges(int ranges) -> nil
    ---@param speed number
    ---@return boolean|string
    ['setSpeed'] = function(speed) end, -- gg.setSpeed(double speed) -> true || string with error
    ---@param values table
    ---@return boolean|string
    ['setValues'] = function(values) end, -- gg.setValues(table values) -> true || string with error
    ---@param visible boolean
    ---@return nil
    ['setVisible'] = function(visible) private.visible = visible return nil end, -- gg.setVisible(bool visible) -> nil
    ---@return nil
    ['showUiButton'] = function() return nil end, -- gg.showUiButton() -> nil
    ---@return nil
    ['skipRestoreState'] = function() return nil end, -- gg.skipRestoreState() -> nil
    ---@param milliseconds number
    ---@return nil
    ['sleep'] = function(milliseconds) return nil end, -- gg.sleep(int milliseconds) -> nil
    ---@param type number
    ---@param memoryFrom number
    ---@param memoryTo number
    ---@param limit number
    ---@return boolean|string
    ['startFuzzy'] = function(type, memoryFrom, memoryTo, limit) end, -- gg.startFuzzy([int type = gg.TYPE_AUTO [, long memoryFrom = 0 [, long memoryTo = -1 [, long limit = 0]]]]) -> true || string with error
    ---@param time string
    ---@return boolean|string
    ['timeJump'] = function(time) end, -- gg.timeJump(string time) -> true || string with error
    ---@param text string
    ---@param fast boolean
    ---@return nil
    ['toast'] = function(text, fast)
        print("[Toast] " .. tostring(text))
        return nil
    end, -- gg.toast(string text [, bool fast = false]) -> nil
    ---@param qword number
    ---@param qincr number
    ---@param double_ number
    ---@param dincr number
    ---@return boolean|string
    ['unrandomizer'] = function(qword, qincr, double_, dincr) end -- gg.unrandomizer([long qword = nil [, long qincr = nil [, double double_ = nil [, double dincr = nil]]]]) -> true || string with error
}