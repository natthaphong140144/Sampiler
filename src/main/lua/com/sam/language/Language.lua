---@class Lang
Language = {}

local this = Language;
local avalibleLanguage = {
    { "ZH_CN_T", "繁體中文" },
    { "ZH_CN_S", "简体中文" },
    { "ENG", "ENGLISH" }
}

function this:selectLanguage()
    local tmpChoice = {}
    local cacheUseable = {}
    local count = #avalibleLanguage
    for i = 1, count do
        cacheUseable[i] = pcall(require, "language." .. avalibleLanguage[i][1]) and "O" or "X"
        local avalible = "[" .. cacheUseable[i] .. "] "
        tmpChoice[i] = avalible .. avalibleLanguage[i][2]
    end
    while true do
        SetUIVisibility(false)
        local sel = Choice(tmpChoice, nil, "Language")
        SetUIVisibility(true)
        if not sel then
            return false
        elseif cacheUseable[sel] == "X" then
            Toast("This language is unavalible")
        elseif cacheUseable[sel] == "O" then
            Config:addSettings("lang", avalibleLanguage[sel][1]):saveConfig()
            break
        end
    end
end

function this:reSelectLang()
    local origLang = Config.Settings.lang
    if this:selectLanguage() == false then
        return
    elseif origLang == Config.Settings.lang then
        return
    end
    SetUIVisibility(false)
    local try, langPack = pcall(require, "language." .. Config.Settings.lang)
    if not try then
        Toast("Cannot load language, switched back.")
        Config:addSettings("lang", origLang):saveConfig()
        return
    else
        Language = langPack
        Language.reSelectLang = this.reSelectLang;
    end
end

function this:init()
    if not Config.Settings.lang then
        if this:selectLanguage() == false then
            os.exit()
        end
    end
    local try, langPack = pcall(require, "language." .. Config.Settings.lang)
    if not try then
        SetUIVisibility(false)
        Toast("Cannot load language, please re-select language.")
        Config.Settings.lang = nil
        this:init()
        return
    else
        Language = langPack
        Language.reSelectLang = this.reSelectLang;
    end
end